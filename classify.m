net = load('C:\Users\Tom\Downloads\matconvnet\MatConvNet\examples\mnist\data\letters-baseline\net-epoch-60.mat') ;
imdb = load('C:\Users\Tom\Downloads\matconvnet\MatConvNet\examples\mnist\data\letters-baseline\imdb.mat') ;

f1=fopen('C:\Users\Tom\Desktop\matlab\lettersDataDip_3_80_v_e60.txt','wt');
f2=fopen('C:\Users\Tom\Desktop\matlab\lettersPredictDip_3_80_v_e60.txt','wt');

images = dir('E:\letters\70-15-15\test\*.jpg');

for i=1:length(images)
   filename = strcat('E:\letters\70-15-15\test\',images(i).name);
    im = imread(filename);
    
    %%%%%%
    fileName = images(i).name;
    shortName = fileName(:,1);
    disp(shortName);
    unicode = unicode2native(shortName, 'windows-1255');
    fprintf(f1,'%d\n',unicode);
    
    %%%%%%%
    im = imcomplement(im);
    %     imshow(im);
    [rows, columns, numberOfColorChannels] = size(im);
    if numberOfColorChannels > 1 %rgb image
        im2 = rgb2gray(im); %convet the image to gray
    else
        im2 = im; %already gray
    end
    level = graythresh(im2);
    im2 = im2bw(im2, level);
    im_ = single(im2);
    
    im_ = imresize(im_, [28 28]);
    im_ = im_ - imdb.images.data_mean;
    net.net.layers{end}.type = 'softmax';
    res = vl_simplenn(net.net, im_);
    scores = squeeze(gather(res(end).x));
    [bestScore, best] = max(scores);
    temp = net.net.meta.classes.name{best};
    temp2= str2double(temp);
    predict = temp2-1;
  
    switch predict
        case 0
            label = 48;
        case 1
            label = 49;
        case 2
            label = 50;
        case 3
            label = 51;
        case 4
            label = 52;
        case 5
            label = 53;
        case 6
            label = 54;
        case 7
            label = 55;
        case 8
            label = 56;
        case 9
            label = 57;
        case 10
            label = 115; %s
            
        otherwise
            label = predict+213;
    end
    
    unlabel = native2unicode(label, 'windows-1255');
    disp(unlabel);
    fprintf(f2,'%d\n',label);
end




fclose(f1);
fclose(f2);