function [net, info] = cnn_letters(varargin)
% CNN_MNIST  Demonstrated MatConNet on MNIST

run(fullfile(fileparts(mfilename('fullpath')),...
    '..', '..', 'matlab', 'vl_setupnn.m')) ;

opts.expDir = fullfile('data','letters-baseline') ;
[opts, varargin] = vl_argparse(opts, varargin) ;

opts.dataDir = 'data/letters' ;
opts.imdbPath = fullfile(opts.expDir, 'imdb.mat');
opts.useBatchNorm = false ;
opts.networkType = 'simplenn' ;
opts.train = struct() ;
[opts, varargin] = vl_argparse(opts, varargin) ;

% --------------------------------------------------------------------
%                                                         Prepare data
% --------------------------------------------------------------------

net = cnn_letters_init('useBatchNorm', opts.useBatchNorm, ...
    'networkType', opts.networkType) ;

if exist(opts.imdbPath, 'file')
    imdb = load(opts.imdbPath) ;
else
    imdb = getMnistImdb(opts) ;
    mkdir(opts.expDir) ;
    save(opts.imdbPath, '-struct', 'imdb') ;
end

net.meta.classes.name = arrayfun(@(x)sprintf('%d',x),1:38,'UniformOutput',false) ;

% --------------------------------------------------------------------
%                                                                Train
% --------------------------------------------------------------------

switch opts.networkType
    case 'simplenn', trainfn = @cnn_train ;
    case 'dagnn', trainfn = @cnn_train_dag ;
end

[net, info] = trainfn(net, imdb, getBatch(opts), ...
    'expDir', opts.expDir, ...
    net.meta.trainOpts, ...
    opts.train, ...
    'val', find(imdb.images.set == 3)) ;

% --------------------------------------------------------------------
function fn = getBatch(opts)
% --------------------------------------------------------------------
switch lower(opts.networkType)
    case 'simplenn'
        fn = @(x,y) getSimpleNNBatch(x,y) ;
    case 'dagnn'
        bopts = struct('numGpus', numel(opts.train.gpus)) ;
        fn = @(x,y) getDagNNBatch(bopts,x,y) ;
end

% --------------------------------------------------------------------
function [images, labels] = getSimpleNNBatch(imdb, batch)
% --------------------------------------------------------------------
images = imdb.images.data(:,:,:,batch) ;
labels = imdb.images.labels(1,batch) ;

% --------------------------------------------------------------------
function inputs = getDagNNBatch(opts, imdb, batch)
% --------------------------------------------------------------------
images = imdb.images.data(:,:,:,batch) ;
labels = imdb.images.labels(1,batch) ;
if opts.numGpus > 0
    images = gpuArray(images) ;
end
inputs = {'input', images, 'label', labels} ;

% --------------------------------------------------------------------
function imdb = getMnistImdb(opts)
% --------------------------------------------------------------------


feature('DefaultCharacterSet');
images = dir('E:\letters\70-15-15\train\*.jpg');
images = images(randperm(length(images)));
imArray = zeros(28,28,length(images));
lblArray = zeros(1,length(images));
for i = 1:length(images)
    filename = strcat('E:\letters\70-15-15\train\',images(i).name);
    
    myim = imread(filename);
    
    im = imcomplement(myim);
    [rows, columns, numberOfColorChannels] = size(im);
    if numberOfColorChannels > 1 %rgb image
        im2 = rgb2gray(im); %convet the image to gray
    else
        im2 = im; %already gray
    end
    
    level = graythresh(im2);
    im2 = im2bw(im2, level);
    
    im_ = imresize(im2, [28 28]);
    
    imArray(:,:,i) = im_;
    
    fileName = images(i).name;
    shortName = fileName(:,1);
    
    label2 = unicode2native(shortName, 'windows-1255');
    
    switch label2
        case 48
            label3 = 0;
        case 49
            label3 = 1;
        case 50
            label3 = 2;
        case 51
            label3 = 3;
        case 52
            label3 = 4;
        case 53
            label3 = 5;
        case 54
            label3 = 6;
        case 55
            label3 = 7;
        case 56
            label3 = 8;
        case 57
            label3 = 9;
        case 115
            label3 = 10; %s
        otherwise
            label3 = label2-213; %letters
         
    end
    lblArray(1,i) = label3+1;
end
x1= imArray;
y1= lblArray;


% images2 = dir('E:\letters\70-15-15\validation\*.jpg');
% images2 = images2(randperm(length(images2)));
% imArray2 = zeros(28,28,length(images2));
% lblArray2 = zeros(1,length(images2));
% for i = 1:length(images2)
%     filename = strcat('E:\letters\70-15-15\validation\',images2(i).name);
%     
%     myim = imread(filename);
%     im = imcomplement(myim);
%     [rows, columns, numberOfColorChannels] = size(im);
%     if numberOfColorChannels > 1 %rgb image
%         im2 = rgb2gray(im); %convet the image to gray
%     else
%         im2 = im; %already gray
%     end
%     level = graythresh(im2);
%     im2 = im2bw(im2, level);
%     
%     im_ = imresize(im2, [28 28]);
%     
%     imArray2(:,:,i) = im_;
%     
%     fileName = images2(i).name;
%     shortName = fileName(:,1);
%     label2 = unicode2native(shortName, 'windows-1255');
%     
%     %     if (length(label)==2)
%     %         label2 = label(:,2);
%     %     else
%     %         label2 = label;
%     %     end
%     
%     switch label2
%         case 48
%             label3 = 0;
%         case 49
%             label3 = 1;
%         case 50
%             label3 = 2;
%         case 51
%             label3 = 3;
%         case 52
%             label3 = 4;
%         case 53
%             label3 = 5;
%         case 54
%             label3 = 6;
%         case 55
%             label3 = 7;
%         case 56
%             label3 = 8;
%         case 57
%             label3 = 9;
%         case 115
%             label3 = 10; %s
%         otherwise
%             label3 = label2-213; %letters
%     end
%     lblArray2(1,i) = label3+1;
%     
% end
% x2= imArray2;
% y2= lblArray2;

set = [ones(1,4262) 3*ones(1,914)];
% set = [ones(1,numel(y1)) 3*ones(1,numel(y2))];
data = single(reshape(x1,28,28,1,[]));
% data = single(reshape(cat(3, x1, x2),28,28,1,[]));
dataMean = mean(data(:,:,:,set == 1), 4);
data = bsxfun(@minus, data, dataMean) ;

imdb.images.data = data ;
imdb.images.data_mean = dataMean;
imdb.images.labels = y1 ;
% imdb.images.labels = cat(2, y1, y2) ;
imdb.images.set = set ;
imdb.meta.sets = {'train', 'val', 'test'} ;
imdb.meta.classes = arrayfun(@(x)sprintf('%d',x),0:37,'uniformoutput',false) ;
